#include "stdio.h"
#include "stdlib.h"
#include "math.h"
//#include "LinearAlgebra.h"

#define LR 1.f
#define EPS .1f
#define grad_EPS .00001f
#define MAX_STEP 100

inline float pow2(float x){
	return x*x;
}
inline float pow4(float x){
	return pow2(x*x);
}

//a class to fit an ellipse to given data foo

class fit_c{
public:
	fit_c() { m_bValid = false;}
	~fit_c() {}

	bool Fit(int nSamp, float *x_in, float *y_in);              // Returns success, failure
	bool correct(float x_in, float y_in);

	float x() {return m_x_corrected;}
	float y() {return m_y_corrected;}
	
	bool write_fit(bool bTemp = false);						// R/W of calibration values.
	bool read_fit(bool bTemp = false);

private:

	float calib_x(int k) {return m_norm * m_x[k];}
	float calib_y(int k) {return m_norm * m_y[k];}

	float* m_x;	// Normalized copy of fit data
	float* m_y;
	int m_nSamples;

	float m_norm;

	bool initialize_fit();//initialize the parameters with a nice algebraic fit
	bool store_calib_data(int nSamp,float *x_in,float *y_in);
	  
	bool m_bValid;// False if fit fails (by what criteria?)

	//stored, corrected values for x and y compass
	float m_x_corrected;
	float m_y_corrected;

	// eqn. for ellipse is m_xc (x - m_dx)**2 + m_yc (y - m_dy)**2 + m_cc (x - m_dx)(y - m_dy) - 1 = 0
	float m_yc;
	float m_xc;
	float m_cc;
	float m_dx;
	float m_dy;
	// Amount ellipse rotated
	float m_theta;
	float m_sx;
	float m_sy;

	//functions to compute gradients w.r.t. parameters
	float dxc();
	float dyc();
	float dcc();
	float ddy();
	float ddx();
	
	float d2xc();
	float d2yc();
	float d2cc();
	float d2dy();
	float d2dx();

	//function to get error in current Fit
	float calc_error();

	//func to get the coefficient for gradient descent calc.
	float fit_coef(float x, float y);

};
