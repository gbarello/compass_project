#if defined(STANDALONE)
#include "compass_fit.h"
#else
#include "../flightsw/base_system.h"
#endif

bool fit_c::correct(float x,float y){
	//set the corrected values using the calibration parameters

	float xt = (cos(m_theta)*((x/m_norm) - m_dx) - sin(m_theta)*((y/m_norm) - m_dy))*sqrt(m_sx);
	float yt = (cos(m_theta)*((y/m_norm) - m_dy) + sin(m_theta)*((x/m_norm) - m_dx))*sqrt(m_sy);

	m_x_corrected = (xt * cos(-m_theta) - yt * sin(-m_theta)) * m_norm;
	m_y_corrected = (yt * cos(-m_theta) + xt * sin(-m_theta)) * m_norm;

	return true;
}

bool fit_c::initialize_fit(){

	//initialize the fit using the saved, normalized, calibration values

	float xycor = 0;
	float xvar = 0;
	float yvar = 0;


	//get the variance, and covariance, which will be used to initialize the ellipse
	for(int k = 0;k<m_nSamples;k++){
		xvar += pow2(m_x[k] - m_dx);
		yvar += pow2(m_y[k] - m_dy);
		xycor += (m_x[k] - m_dx)*(m_y[k] - m_dy);
	};

	xycor /= (m_nSamples - 1);
	xvar /= (m_nSamples - 1);
	yvar /= (m_nSamples - 1);

	//the x and y scales are half the variance
	m_yc = .5f/yvar;
	m_xc = .5f/xvar;

	//the cross term is (-) the covariance scaled by the variances.
	m_cc = -xycor/(xvar*yvar);

	return true;
}

bool fit_c::Fit(int nSamp, float *x_in, float *y_in)
{
	if (nSamp < 5 || x_in == NULL || y_in == NULL) {
		m_bValid = false;
		return m_bValid;
	}
	m_nSamples = nSamp;
	m_norm = 0;

	m_x = (float*) malloc(m_nSamples*sizeof(float));  // Save a normalized copy of the data for fitting
	m_y = (float*) malloc(m_nSamples*sizeof(float));

	m_dx = 0;//zero the averages
	m_dy = 0;

	for(int k = 0;k<m_nSamples;k++){//store the data, and calculate mean
		m_x[k] = x_in[k];
		m_y[k] = y_in[k];

		m_dx += m_x[k];
		m_dy += m_y[k];

	}

	m_dx/=m_nSamples;
	m_dy/=m_nSamples;

	for(int k = 0;k<m_nSamples;k++){//calculate average distance from mean value
		m_norm += sqrt(pow2(m_x[k] - m_dx) + pow2(m_y[k] - m_dy));
	}

	m_norm /= m_nSamples;

	for(int k = 0;k<m_nSamples;k++){
		m_x[k] /= m_norm;
		m_y[k] /= m_norm;
	}

	m_dx /= m_norm; //rescale the mean values, because they are used later on the normalized data.
	m_dy /= m_norm;

	m_bValid = initialize_fit();
	if (m_bValid == false)
		return m_bValid;

	//create some temporary values for fitting
	float error = calc_error();
	float new_error = 0;

	float t_yc;
	float t_xc;
	float t_cc;
	float t_dx;
	float t_dy;

	float t_2yc;
	float t_2xc;
	float t_2cc;
	float t_2dx;
	float t_2dy;

	int step;
	for(step = 0;step <= MAX_STEP;step += 1){
		//compiute all teh gradients

		t_yc = dyc();
		t_xc = dxc();
		t_cc = dcc();
		t_dx = ddx();
		t_dy = ddy();

		t_2yc = d2yc();
		t_2xc = d2xc();
		t_2cc = d2cc();
		t_2dx = d2dx();
		t_2dy = d2dy();

		//take a step in the right direction
		m_yc -= LR*t_yc / (t_2yc + EPS);
		m_xc -= LR*t_xc / (t_2xc + EPS);
		m_cc -= LR*t_cc / (t_2cc + EPS);
		m_dx -= LR*t_dx / (t_2dx + EPS);
		m_dy -= LR*t_dy / (t_2dy + EPS);

		new_error = calc_error();

		if(fabs(new_error - error) < grad_EPS){
#if defined(STANDALONE)
			printf("break at: %d\n",step);
#endif
			break;
		}
		error = new_error;
	}
	if(step == MAX_STEP){
		m_bValid = false;
	}


	//set parameters of the transformation
	float newY = m_cc*(m_yc - m_xc)/sqrt(pow4(m_xc-m_yc) + pow2(m_cc)*pow2(m_xc - m_yc) - pow2(m_yc - m_xc)*sqrt(pow4(m_xc - m_yc) + pow2(m_xc - m_yc)*pow2(m_cc)));

	float newX = - sqrt(1.f - (pow2(m_xc - m_yc)/sqrt(pow4(m_xc-m_yc) + pow2(m_xc-m_yc)*pow2(m_cc))));

	//these are rotation/stretch parameters to transform the elipse into a circle.
	m_theta = atan2(newY,newX);

	m_sx = fabs(.5f*(pow2(m_yc) - pow2(m_xc) + sqrt(pow4(m_xc - m_yc) + pow2(m_xc - m_yc)*pow2(m_cc)))/(m_xc - m_yc));
	m_sy = fabs(.5f*(pow2(m_xc) - pow2(m_yc) + sqrt(pow4(m_xc - m_yc) + pow2(m_xc - m_yc)*pow2(m_cc)))/(m_xc - m_yc));

	free(m_x);
	free(m_y);

	m_bValid = true;
	return m_bValid;
}

//everything down here is just computations of gradients of various parameters

float fit_c::fit_coef(float x,float y){
	return 2*(-1 + m_xc * pow2((x - m_dx)) + m_yc * pow2((y - m_dy)) + m_cc * (x - m_dx) * (y - m_dy));
}

float fit_c::dxc(){

	float retval = 0;

	for(int k = 0;k<m_nSamples;k++){
		retval += pow2(m_x[k] - m_dx) * fit_coef(m_x[k],m_y[k]);
	}

	return retval/m_nSamples;
}

float fit_c::d2xc(){

	float retval = 0;

	for(int k = 0;k<m_nSamples;k++){
		retval += 2*pow4(m_x[k] - m_dx);
	}

	return retval/m_nSamples;
}

float fit_c::dyc(){

	float retval = 0;

	for(int k = 0;k<m_nSamples;k++){
		retval += pow2(m_y[k] - m_dy) * fit_coef(m_x[k],m_y[k]);
	}

	return retval/m_nSamples;

}
float fit_c::d2yc(){

	float retval = 0;

	for(int k = 0;k<m_nSamples;k++){
		retval += 2*pow4(m_y[k] - m_dy);
	}

	return retval/m_nSamples;

}
float fit_c::dcc(){

	float retval = 0;

	for(int k = 0;k<m_nSamples;k++){
		retval += (m_x[k] - m_dx) * (m_y[k] - m_dy) * fit_coef(m_x[k],m_y[k]);
	}

	return retval/m_nSamples;

}
float fit_c::d2cc(){

	float retval = 0;

	for(int k = 0;k<m_nSamples;k++){
		retval += 2*pow2(m_x[k] - m_dx) * pow2(m_y[k] - m_dy);
	}

	return retval/m_nSamples;

}
float fit_c::ddx(){

	float retval = 0;

	for(int k = 0;k<m_nSamples;k++){
		retval += (- 2* m_xc * (m_x[k] - m_dx) - m_cc * (m_y[k] - m_dy))* fit_coef(m_x[k],m_y[k]);
	}

	return retval/m_nSamples;

}
float fit_c::d2dx(){

	float retval = 0;

	for(int k = 0;k<m_nSamples;k++){
		retval += 2*pow2( - 2 * m_xc * (m_x[k] - m_dx) - m_cc * (m_y[k] - m_dy)) + 4 * m_xc * fit_coef(m_x[k],m_y[k]);
	}


	return retval/m_nSamples;

}
float fit_c::ddy(){

	float retval = 0;

	for(int k = 0;k<m_nSamples;k++){
		retval += (- 2* m_yc * (m_y[k] - m_dy) - m_cc * (m_x[k] - m_dx))* fit_coef(m_x[k],m_y[k]);
	}

	return retval/m_nSamples;
}

float fit_c::d2dy(){

	float retval = 0;

	for(int k = 0;k<m_nSamples;k++){
		retval += 2*pow2( - 2 * m_yc * (m_y[k] - m_dy) - m_cc * (m_x[k] - m_dx)) + 4 * m_yc * fit_coef(m_x[k],m_y[k]);
	}

	return retval/m_nSamples;
}

//calculate the error in the fit.

float fit_c::calc_error(){

	float err = 0;

	for(int k = 0;k < m_nSamples;k++){
		err += pow2(m_xc * pow2(m_x[k] - m_dx) + m_yc * pow2(m_y[k] - m_dy) + m_cc * (m_y[k] - m_dy)*(m_x[k] - m_dx));
	}

	err /= m_nSamples;

	return err;

}

#if !defined(STANDALONE)

bool fit_c::write_fit(bool bTemp) {
	FILE *fp = fopen(bTemp?IMU_MAGCAL_TEMP:IMU_MAGCAL_DEST, "w"); 
	if (fp == NULL)
	{
		printf("<html>Write MagCal failed: %s</html>\n", strerror(errno));
		return false;
	}
	else
	{
		fprintf(fp, "%f, %f, %f, %f, %f, %f, %i\n", 
			m_norm,
			m_dx,
			m_dy,
			m_theta,
			m_sx,
			m_sy,
			m_bValid
			);
	}
	fclose(fp);

	return m_bValid;		// Return state of cal
}

bool fit_c::read_fit(bool bTemp) {
	FILE *fp = fopen(bTemp?IMU_MAGCAL_TEMP:IMU_MAGCAL_DEST, "r"); 
	if (fp == NULL)
	{
//		printf("<html>Read MagCal failed: %s</html>\n", strerror(errno));
		return false;
	}
	else
	{
		int valid;
		int n = fscanf(fp, "%f, %f, %f, %f, %f, %f, %i",
			&m_norm,
			&m_dx,
			&m_dy,
			&m_theta,
			&m_sx,
			&m_sy,
			&valid
			);
		// convert int to bool and combine to determine if we have enough
		// info to perform the elliptical correction.
		m_bValid = (n == 7) && valid?true:false;
	}
	fclose(fp);

	return m_bValid;
}

#endif	// !defined(STANDALONE)